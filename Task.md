# Dockerfile
---

## Task Description
- Create a Dockerfile for the service
- Create an empty public repository for the service in quay.io (into the "bootcampseladevops" organization)

## Considerations
- This Dockerfile will be used in the jenkinsfile task IN the CI pipeline.
- Write a Dockerfile that builds small images.
- Write a Dockerfile that builds as fast as possible.

## Merging
- To merge this branch into master you must do it via Pull Request
- The Pull Request must be reviewed and approved from another member of the team
